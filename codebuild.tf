resource "aws_codebuild_project" "ecr" {
  artifacts {
    type = "CODEPIPELINE"
  }
  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    environment_variable {
      name = "AWS_ACCOUNT_ID"
      value = data.aws_caller_identity.current.account_id
    }
    image                       = "aws/codebuild/amazonlinux2-x86_64-standard:2.0"
    privileged_mode             = "true"
    type                        = "LINUX_CONTAINER"
    image_pull_credentials_type = "CODEBUILD"
  }
  logs_config {
    cloudwatch_logs {
      group_name = aws_cloudwatch_log_group.ecr.name
    }
  }
  name          = "ecr-${var.repo-name}"
  service_role  = data.terraform_remote_state.codepipeline.outputs.codebuild-role-arn
  source {
    type = "CODEPIPELINE"
  }
  tags = {
    Name   = "ecr-${var.repo-name}"
  }
}
