resource "aws_codepipeline" "ecr" {
  name     = "ecr-${var.repo-name}"
  role_arn = data.terraform_remote_state.codepipeline.outputs.codepipeline-role-arn

  artifact_store {
    location = data.terraform_remote_state.codepipeline.outputs.codepipeline-artifacts-bucket-id
    type     = "S3"
  }

  stage {
    name = "Source"

    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "CodeCommit"
      version          = "1"
      output_artifacts = ["SourceArti"]
      configuration = {
        RepositoryName = aws_ecr_repository.ecr.name
        BranchName     = "master"
      }
    }
  }

  stage {
    name = "Build"

    action {
      name             = "Build"
      category         = "Build"
      owner            = "AWS"
      provider         = "CodeBuild"
      input_artifacts  = ["SourceArti"]
      output_artifacts = ["BuildArti"]
      version          = "1"

      configuration = {
        ProjectName = aws_codebuild_project.ecr.name
      }
    }
  }
}
