resource "aws_ecr_repository" "ecr" {
  name  = var.repo-name
  tags = {
    Name = var.repo-name
  }
}
