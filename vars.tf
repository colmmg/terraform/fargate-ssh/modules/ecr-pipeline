variable "region" {
  description = "The AWS region."
}

variable "repo-name" {
  description = "The name of the ECR repository to create."
}
