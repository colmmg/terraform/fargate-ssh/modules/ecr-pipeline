resource "aws_cloudwatch_log_group" "ecr" {
  name              = "/aws/codebuild/ecr-${var.repo-name}"
  retention_in_days = "30"
  tags = {
    Name = var.repo-name
  }
}
