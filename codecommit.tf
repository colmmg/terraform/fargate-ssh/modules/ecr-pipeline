resource "aws_codecommit_repository" "ecr" {
  repository_name = var.repo-name
  tags = {
    Name = "${var.repo-name}"
  }
}
