data "aws_caller_identity" "current" {}

data "terraform_remote_state" "codepipeline" {
  backend = "s3"
  config = {
    bucket = "tf-state-${var.region}-${data.aws_caller_identity.current.account_id}"
    key    = "infra/codepipeline/terraform.tfstate"
    region = var.region
  }
}
