# ecr-pipeline
A Terraform module for creating an Amazon [Elastic Container Registry](https://aws.amazon.com/ecr/) and an AWS [CodePipeline](https://aws.amazon.com/codepipeline/) project for building the source code.

# Prerequisites
The Terraform stack at [colmmg/terraform/fargate-ssh/infra/codepipeline](https://gitlab.com/colmmg/terraform/fargate-ssh/infra/codepipeline) needs to be created first with the Terraform remote state of that stack being deployed to S3 bucket `tf-state-$AWS_REGION-$AWS_ACCOUNT_ID` and key `infra/codepipeline/terraform.tfstate`.
